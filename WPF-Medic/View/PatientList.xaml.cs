﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Medic.View
{
    /// <summary>
    /// Logique d'interaction pour PatientList.xaml
    /// </summary>
    public partial class PatientList : UserControl, ISwitchable
    {

        public PatientList(string login)
        {
            InitializeComponent();
            ViewModel.PatientListViewModel vm = new ViewModel.PatientListViewModel(login);
            this.DataContext = vm;
        }

        public void UtilizeState(object state)
        {
            throw new NotImplementedException();
        }


    }
}
