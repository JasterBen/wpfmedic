﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Medic.View
{
    /// <summary>
    /// Logique d'interaction pour UserList.xaml
    /// </summary>
    public partial class UserList : UserControl, ISwitchable
    {
        public UserList(string login)
        {
            InitializeComponent();
            ViewModel.UserListViewModel vm = new ViewModel.UserListViewModel(login);
            this.DataContext = vm;
        }

        public void UtilizeState(object state)
        {
            throw new NotImplementedException();
        }

    }
}
