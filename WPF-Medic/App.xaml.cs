﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace WPF_Medic
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            PageSwitcher window = new PageSwitcher();

            /*ViewModel.MainViewModel vm = new ViewModel.MainViewModel();
            window.DataContext = vm;*/

            window.Show();
        }
    }
}
