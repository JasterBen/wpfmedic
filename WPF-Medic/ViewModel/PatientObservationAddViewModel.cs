﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPF_Medic.ViewModel
{
    class PatientObservationAddViewModel : BaseViewModel
    {
        #region variable
        private ServiceObservation.ServiceObservationClient _observationService = new ServiceObservation.ServiceObservationClient();

        private int id;

        private DateTime _inputDate;
        private string _inputComment;
        private int _inputBlood;
        private int _inputWeight;
        private string _inputPrescription;
        private ObservableCollection<String> _prescriptionList;
        private byte[] _inputPicture;
        private ObservableCollection<byte[]> _pictureList;

        private ICommand _addPrescriptionCommand;
        private ICommand _addImageCommand;
        private ICommand _validateObservationCommand;
        #endregion


        #region getter setter
        public DateTime InputDate
        {
            get { return _inputDate; }
            set
            {
                _inputDate = value;
                OnPropertyChanged("InputDate");
            }
        }

        public string InputComment
        {
            get { return _inputComment; }
            set
            {
                _inputComment = value;
                OnPropertyChanged("InputComment");
            }
        }

        public int InputWeight
        {
            get { return _inputWeight; }
            set
            {
                _inputWeight = value;
                OnPropertyChanged("InputWeight");
            }
        }

        public int InputBlood
        {
            get { return _inputBlood; }
            set
            {
                _inputBlood = value;
                OnPropertyChanged("InputBlood");
            }
        }

        public string InputPrescription
        {
            get { return _inputPrescription; }
            set
            {
                _inputPrescription = value;
                OnPropertyChanged("InputPrescription");
            }
        }

        public ObservableCollection<string> PrescriptionList
        {
            get { return _prescriptionList; }
            set
            {
                _prescriptionList = value;
                OnPropertyChanged("PrescriptionList");
            }
        }

        public byte[] InputPicture
        {
            get { return _inputPicture; }
            set
            {
                _inputPicture = value;
                OnPropertyChanged("InputPicture");
            }
        }

        public ObservableCollection<byte[]> PictureList
        {
            get { return _pictureList; }
            set
            {
                _pictureList = value;
                OnPropertyChanged("PictureList");
            }
        }

        public ICommand AddPrescriptionCommand
        {
            get { return _addPrescriptionCommand; }
            set
            {
                _addPrescriptionCommand = value;
                OnPropertyChanged("AddPrescriptionCommand");
            }
        }

        public ICommand AddImageCommand
        {
            get { return _addImageCommand; }
            set
            {
                _addImageCommand = value;
                OnPropertyChanged("AddImageCommand");
            }
        }

        public ICommand ValidateObservationCommand
        {
            get { return _validateObservationCommand; }
            set
            {
                _validateObservationCommand = value;
                OnPropertyChanged("ValidateObservationCommand");
            }
        }
        #endregion


        public PatientObservationAddViewModel(string login, int id) : base(login)
        {
            this.id = id;

            _prescriptionList = new ObservableCollection<string>();
            _pictureList = new ObservableCollection<byte[]>();

            _addPrescriptionCommand = new RelayCommand(param => AddPrescription(), param => CanAddPrescription);
            _addImageCommand = new RelayCommand(param => AddImage(), param => true);
            _validateObservationCommand = new RelayCommand(param => ValidateObservation(), param => CanValidate);
        }


        private Boolean CanAddPrescription
        {
            get { return !String.IsNullOrEmpty(_inputPrescription); }
        }

        private Boolean CanValidate
        {
            get { return _inputBlood != 0 &&
                    _inputDate != null &&
                    _inputWeight != 0 &&
                    !String.IsNullOrEmpty(_inputComment) &&
                    _prescriptionList.Count != 0; }
        }


        private void AddPrescription()
        {
            _prescriptionList.Add(_inputPrescription);
            InputPrescription = null;
            PrescriptionList = _prescriptionList;
        }


        private void AddImage()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                string filename = dialog.FileName;
                _inputPicture = File.ReadAllBytes(filename);
                _pictureList.Add(_inputPicture);
                PictureList = _pictureList;
            }
        }


        private void ValidateObservation()
        {
            ServiceObservation.Observation obs = new ServiceObservation.Observation();
            obs.Date = _inputDate;
            obs.Comment = _inputComment;
            obs.BloodPressure = _inputBlood;
            obs.Weight = _inputWeight;
            obs.Pictures = _pictureList.ToArray();
            obs.Prescription = _prescriptionList.ToArray();

            _observationService.AddObservation(id, obs);
            Switcher.Switch(new View.PatientDataList(_userLogged, this.id));
        }


        protected override void Back()
        {
            Switcher.Switch(new View.PatientDataList(_userLogged, this.id));
        }
    }
}
