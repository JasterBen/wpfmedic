﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPF_Medic.ViewModel
{
    class PatientDataListViewModel : BaseViewModel, ServiceLive.IServiceLiveCallback
    {
        #region variables
        private ServiceObservation.ServiceObservationClient observationService = new ServiceObservation.ServiceObservationClient();
        private ServicePatient.ServicePatientClient patientService = new ServicePatient.ServicePatientClient();
        private ServiceLive.ServiceLiveClient liveService;

        private ServicePatient.Observation _selectedObservation;
        private ObservableCollection<ServicePatient.Observation> _observationList;

        private ICommand _addObservationCommand;

        private double _dataHeart;
        private double _dataTemp;

        private BackgroundWorker _bgworker = new BackgroundWorker();
        #endregion

        #region getter setter
        public ServicePatient.Observation SelectedObservation
        {
            get { return _selectedObservation; }
            set
            {
                _selectedObservation = value;
                OnPropertyChanged("SelectedObservation");
            }
        }

        public ObservableCollection<ServicePatient.Observation> ObservationList
        {
            get { return _observationList; }
            set
            {
                _observationList = value;
                OnPropertyChanged("ObservationList");
            }
        }

        public ICommand AddObservationCommand
        {
            get { return _addObservationCommand; }
            set
            {
                _addObservationCommand = value;
                OnPropertyChanged("AddObservationCommand");
            }
        }

        public double DataHeart
        {
            get { return _dataHeart; }
            set
            {
                _dataHeart = value;
                OnPropertyChanged("DataHeart");
            }
        }

        public double DataTemp
        {
            get { return _dataTemp; }
            set
            {
                _dataTemp = value;
                OnPropertyChanged("DataTemp");
            }
        }
        #endregion


        public PatientDataListViewModel(int id, string login) : base(login)
        {
            liveService = new ServiceLive.ServiceLiveClient(new InstanceContext(this));
            _bgworker.DoWork += new DoWorkEventHandler((s, e) =>
            {
                liveService.Subscribe();
            });
            _bgworker.RunWorkerAsync();

            ServicePatient.Patient patient = patientService.GetPatient(id);
            List<ServicePatient.Observation> list = patientService.GetPatient(id).Observations.ToList();
            _observationList = new ObservableCollection<ServicePatient.Observation>(patientService.GetPatient(id).Observations.ToList());

            _addObservationCommand = new RelayCommand(param => AddObservation(id), param => true);
        }


        public void AddObservation(int id)
        {
            Switcher.Switch(new View.PatientObservationAdd(id, _userLogged));
        }


        protected override void Back()
        {
            Switcher.Switch(new View.PatientList(_userLogged));
        }

        public void PushDataHeart(double requestData)
        {
            DataHeart = requestData;
        }

        public void PushDataTemp(double requestData)
        {
            DataTemp = requestData;
        }
    }
}
