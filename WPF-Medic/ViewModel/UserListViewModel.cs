﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace WPF_Medic.ViewModel
{
    class UserListViewModel : BaseViewModel
    {
        #region variables
        private ServiceUser.ServiceUserClient userService = new ServiceUser.ServiceUserClient();
        private ServiceUser.User _selectedUser;
        private List<ServiceUser.User> _userList;

        private string _login;
        private string _pwd;
        private string _name;
        private string _firstname;
        private byte[] _picture;
        private string _role;

        private ICommand _addUserCommand;
        private ICommand _deleteUserCommand;
        private ICommand _getPictureCommand;

        private bool _isNotNurse;
        #endregion

        public bool IsNotNurse
        {
            get { return _isNotNurse; }
            set
            {
                _isNotNurse = value;
                OnPropertyChanged("IsNotNurse");
            }
        }

        #region getter setter
        public ServiceUser.User SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                _selectedUser = value;
                OnPropertyChanged("SelectedUser");
            }
        }

        public List<ServiceUser.User> UserList
        {
            get { return _userList; }
            set
            {
                _userList = value;
                OnPropertyChanged("UserList");
            }
        }

        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public string Pwd
        {
            get { return _pwd; }
            set { _pwd = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        public byte[] Picture
        {
            get { return _picture; }
            set { _picture = value; }
        }

        public string Role
        {
            get { return _role; }
            set { _role = value; }
        }

        public ICommand AddUserCommand
        {
            get { return _addUserCommand; }
            set
            {
                _addUserCommand = value;
                OnPropertyChanged("AddUserCommand");
            }
        }

        public ICommand DeleteUserCommand
        {
            get { return _deleteUserCommand; }
            set
            {
                _deleteUserCommand = value;
                OnPropertyChanged("DeleteUserCommand");
            }
        }

        public ICommand GetPictureCommand
        {
            get { return _getPictureCommand; }
            set
            {
                _getPictureCommand = value;
                OnPropertyChanged("GetPictureCommand");
            }
        }

        #endregion


        public UserListViewModel(string login) : base(login)
        {
            _userList = userService.GetListUser().ToList();
            IsNotNurse = (bool)App.Current.Properties["IsNotNurse"];

            _addUserCommand = new RelayCommand(param => AddUser(), param => IsNotNurse);
            _deleteUserCommand = new RelayCommand(param => DeleteUser(), param => CanDeleteUser && IsNotNurse);
            _getPictureCommand = new RelayCommand(param => GetPicture(), param => true);
        }


        public Boolean CanDeleteUser
        {
            get { return _selectedUser != null; }
        }


        protected override void Back()
        {
            Switcher.Switch(new View.PostLogin(_userLogged));
        }


        private void GetPicture()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                string filename = dialog.FileName;
                _picture = File.ReadAllBytes(filename); 
            }
        }


        private void AddUser()
        {
            try
            {
                ServiceUser.User user = new ServiceUser.User();
                user.Login = _login;
                user.Pwd = _pwd;
                user.Name = _name;
                user.Firstname = _firstname;
                user.Picture = _picture;
                user.Role = _role;
                userService.AddUser(user);
                UserList = userService.GetListUser().ToList();
            } catch (ProtocolException e)
            {
                MessageBox.Show("Image too big. Can not add user.");
            }
            
        }


        private void DeleteUser()
        {
            userService.DeleteUser(_selectedUser.Login);
            UserList = userService.GetListUser().ToList();
        }
    }
}
