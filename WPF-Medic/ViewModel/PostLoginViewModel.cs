﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPF_Medic.ViewModel
{
    class PostLoginViewModel : BaseViewModel
    {
        private ServiceUser.ServiceUserClient _userService = new ServiceUser.ServiceUserClient();

        private ICommand _seeUserListCommand;
        private ICommand _seePatientListCommand;
        private ICommand _logoutCommand;

        public ICommand SeeUserListCommand
        {
            get { return _seeUserListCommand; }
            set { _seeUserListCommand = value; }
        }

        public ICommand SeePatientListCommand
        {
            get { return _seePatientListCommand; }
            set { _seePatientListCommand = value; }
        }

        public ICommand LogoutCommand
        {
            get { return _logoutCommand; }
            set { _logoutCommand = value; }
        }


        public PostLoginViewModel(string login) : base(login)
        {
            _seeUserListCommand = new RelayCommand(param => SeeUserList(), param => true);
            _seePatientListCommand = new RelayCommand(param => SeePatientList(), param => true);
            _logoutCommand = new RelayCommand(param => Logout(), param => true);
        }

        private void SeeUserList()
        {
            Switcher.Switch(new View.UserList(_userLogged));
        }

        private void SeePatientList()
        {
            Switcher.Switch(new View.PatientList(_userLogged));
        }

        private void Logout()
        {
            _userService.Disconnect(_userLogged);
            Switcher.Switch(new View.Login());
        }

        protected override void Back()
        {
            return;
        }
    }
}
