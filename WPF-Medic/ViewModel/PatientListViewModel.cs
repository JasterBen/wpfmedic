﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPF_Medic.ViewModel
{
    class PatientListViewModel : BaseViewModel
    {
        #region variable
        private ServicePatient.ServicePatientClient patientService = new ServicePatient.ServicePatientClient();
        private ServicePatient.Patient _selectedPatient;
        private List<ServicePatient.Patient> _patientList;

        private string _name;
        private string _firstname;
        private DateTime _birthday;

        private ICommand _deletePatientCommand;
        private ICommand _seePatientObservationCommand;
        private ICommand _addPatientCommand;

        private bool _isNotNurse;
        #endregion

        public bool IsNotNurse
        {
            get { return _isNotNurse; }
            set
            {
                _isNotNurse = value;
                OnPropertyChanged("IsNotNurse");
            }
        }


        #region getter setter
        public ServicePatient.Patient SelectedPatient
        {
            get { return _selectedPatient; }
            set
            {
                _selectedPatient = value;
                OnPropertyChanged("SelectedPatient");
            }
        }

        public List<ServicePatient.Patient> PatientList
        {
            get { return _patientList; }
            set
            {
                _patientList = value;
                OnPropertyChanged("PatientList");
            }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        public DateTime Birthday
        {
            get { return _birthday; }
            set { _birthday = value; }
        }

        public ICommand DeletePatientCommand
        {
            get
            { return _deletePatientCommand; }
            set { _deletePatientCommand = value; }
        }

        public ICommand SeePatientObservationCommand
        {
            get
            { return _seePatientObservationCommand; }
            set { _seePatientObservationCommand = value; }
        }

        public ICommand AddPatientCommand
        {
            get { return _addPatientCommand; }
            set { _addPatientCommand = value; }
        }
        #endregion


        public PatientListViewModel(string login) : base(login)
        {
            _patientList = patientService.GetListPatient().ToList();
            IsNotNurse = (bool)App.Current.Properties["IsNotNurse"];

            _addPatientCommand = new RelayCommand(param => AddPatient(), param => IsNotNurse);
            _deletePatientCommand = new RelayCommand(param => DeletePatient(), param => CanDelete && IsNotNurse);
            _seePatientObservationCommand = new RelayCommand(param => SeePatientObservation(), param => CanSee && IsNotNurse);
        }


        private bool CanDelete
        {
            get { return _selectedPatient != null; }
        }

        private bool CanSee
        {
            get { return _selectedPatient != null; }
        }


        #region command function
        private void DeletePatient()
        {
            patientService.DeletePatient(_selectedPatient.Id);
            PatientList = patientService.GetListPatient().ToList();
        }

        private void SeePatientObservation()
        {
            Switcher.Switch(new View.PatientDataList(_userLogged, _selectedPatient.Id));
        }


        private void AddPatient()
        {
            ServicePatient.Patient patient = new ServicePatient.Patient();
            patient.Name = _name;
            patient.Firstname = _firstname;
            patient.Birthday = _birthday;
            patient.Observations = new List<ServicePatient.Observation>().ToArray();
            patientService.AddPatient(patient);
            PatientList = patientService.GetListPatient().ToList();
        }


        protected override void Back()
        {
            Switcher.Switch(new View.PostLogin(_userLogged));
        }
        #endregion
    }
}
